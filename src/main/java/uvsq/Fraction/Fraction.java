package uvsq.Fraction;

public final class Fraction {
    int denominateur;
    int numerateur;
    public final Fraction ZERO = new Fraction(0,1);
    public final Fraction UN = new Fraction (1,1);

    public Fraction(int numerateur, int denominateur){
        this.denominateur=denominateur;
        this.numerateur=numerateur;
    }

    public Fraction(int numerateur){
        this.numerateur=numerateur;
        this.denominateur=1;
        
    }

    public Fraction(){
        this.denominateur=1;
        this.numerateur=0;
    }

    public int getNumerateur(){
        return this.numerateur;
    }

    public int getDenominateur(){
        return  this.denominateur;
    }

    public double getDouble(){
        return (((double)this.numerateur)/this.denominateur);
    }

    @Override
    public String toString() {
        return ""+this.numerateur+"/"+this.denominateur;
    }

    public  boolean testEqual(Fraction fraction1, Fraction fraction2) {
        if (fraction1.getDouble() == fraction2.getDouble()) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String comparaison(Fraction fraction1, Fraction fraction2){
        if (fraction1.getDouble()>=fraction2.getDouble()){
            return ""+fraction1.toString() + "est plus grand ou égal à " + fraction2.toString();
        }
        else
        {
            return ""+fraction1.toString() + "est plus petit que " + fraction2.toString();
        }
    }
}
